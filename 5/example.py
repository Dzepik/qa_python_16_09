# # tuple
#
# my_tuple = (1, 2.0, True, 'four', [1, 2, 3], None, 1)
#
# print(my_tuple)
# print(type(my_tuple))
#
# print(my_tuple[0])
# print(my_tuple[3:6])
#
#
# my_tuple = (1,)
# print(type(my_tuple))
#
#
# my_tuple = (1, 2.0, True, 'four', [1, 2, 3], None, 1)
# print(type(my_tuple[4]))
# my_tuple[4].append(4)
# print(my_tuple)
#
# # my_tuple = (1, 2.0, True, 'four', [1, 2, 3], None, (1, 2, 4), 1)
#
#
# my_tuple = (1, 2.0, True, 'four', [1, 2, 3], None, 1) + (1, 2, 4)
# print(my_tuple)
#
# my_tuple = 1, 2, 3, 4
# print(type(my_tuple))
#
# print('abcd' in my_tuple)

# my_tuple = (1, 2.0, True, 'four', [1, 2, 3], None, (1, 2, 4), 1)
#
# for elem in my_tuple:
#     print(elem, type(elem))
#
#
# print((1, 2, 3) == (1, 2, 4))
#
#
# for i in range(5):
#     print(i)
#
# my_list = list('qwertyuio')
# print(my_list)
#
# my_tuple = tuple('qwertyuio')
# print(my_tuple)
#
# my_list = list(my_tuple)
# print(my_list)
#
# my_tuple = tuple(range(5))
# print(my_tuple)
#
# my_list = list(range(15, 5, -2))
# print(my_list)


# # set
#
# my_set = {1, 2.0, 'qwert', None, (1, 2, 3), 1, 2, 3}
#
# print(my_set)
#
# my_set.add('1')
# print(my_set)
# my_set.add('10')
# print(my_set)
#
#
# # my_set.update({1, 2, 3, 4, 5, 6})
# # print(my_set)
#
# my_set.remove('10')
# print(my_set)
#
# print('10' in my_set)
#
# my_set.pop()
# print(my_set)
#
# my_set.pop()
# print(my_set)
#
# print(my_set.pop())
#
#
# for element in my_set:
#     print(element)
#
# my_set = set('qwertqwertqwertyuasdfgherty')
# print(my_set)
#
# my_set = set([1, 2, 3, 1, 2, 4, 1])
# print(my_set)
#
# # frozenset
# my_frset = frozenset([1, 2, 3, 1, 2, 4, 1])
# print(my_frset)

# dict

# my_dict = {
#     'key_1': 1,
#     1: 2,
#     2.2: {1, 2, 3},
#     False: 'qwerty',
#     None: None,
#     (1, 2, 3): [1, 2, 3, 4, 5],
# }
#
# print(my_dict)
# print(my_dict['key_1'])
# print(my_dict[(1, 2, 3)])
#
# print(my_dict['key_1'])
#
#
# print(my_dict)
# my_dict['key_1'] = 1000
# print(my_dict)
#
# my_dict[(1, 2, 3)][0] = '1234'
#
# lst = my_dict[(1, 2, 3)]
# lst[0] = 'asdfghjkl'
#
# print(my_dict)
#
# # my_dict[(1, 2, 3)][100] = 1234567
#
# my_dict['qwertyuio'] = '1234'
# print(my_dict)

# my_dict = {
#     'key_1': 1,
#     1: 2,
#     2.2: {1, 2, 3},
#     False: 'qwerty',
#     None: None,
#     (1, 2, 3): [1, 2, 3, 4, 5],
# }

# print(my_dict)
#
# for i in my_dict:
#     print('--->', i, my_dict[i])


# for i in my_dict.keys():
#     print('--->', i, my_dict[i])


# for i in my_dict.values():
#     print('--->', i)


# for i in my_dict.items():
#     if i[1] > 10:
#         print(i[0])
#     print('--->', i)
#     print('key is ', i[0])
#     print('value is ', i[1])


# for key, value in my_dict.items():
#     print('key is ', key)
#     print('value is ', value)


# my_dict = {
#     'key_1': 1,
#     1: 2222222222,
#     2.2: {1, 2, 3},
#     False: 'qwerty',
#     None: None,
#     (1, 2, 3): [1, 2, 3, 4, 5],
#     # 'key_2': {1, 2, 3, 4}
# }

# res = my_dict.get('key_2', 'value')
# print(res)
# print(my_dict)
#
# res = my_dict.setdefault('key_2', 'value')
# print('--->', res)
#
# print(my_dict)

my_dict = {
    'key_1': {
        '1': 1,
        '2': 2,
    },
    (1, 2): 123,
}

print(my_dict['key_1']['1'])

dict2 = {1: 2}

my_dict.update(dict2)

my_dict['key_2'] = 1234567
# print(my_dict['key_3'])

lst = list(my_dict.keys())
print(lst)

lst = list(my_dict.values())
print(lst)

lst = list(my_dict.items())
print(lst)
