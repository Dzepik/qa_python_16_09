# a = 1
# b = 2
#
# c = a + b  # add a and b and put result into c
#
# print(c)
#
# a = 10
# A = 20
#
# a1 = 30
# # 1a = 30
#
# students_counter = 30  # snake case
# studentsCounter = 30  # camel case
#
#
# # good
#
# # O
# # o
# # l -> I 1
#
#
# # typing
#
# studentsCounter = 'qwertyzxcvb'
#
# studentsCounter = 30

# a = 10
# b = '20'
#
# c = a + b
#
# print(c)


# data types

# int
# float

my_int1 = 10
my_int2 = 3

res = my_int1 + my_int2
print(res)

res = my_int1 - my_int2
print(res)

res = my_int1 * my_int2
print(res)

res = my_int1 / my_int2
print(res)

res = my_int1 ** my_int2
print(res)

res = my_int1 // my_int2
print(res)

res = my_int1 % my_int2  # 10 % 3 -> 10 - 3 * 3
print(res)

res = 4 % 2
print(res)


my_int1 = my_int1 + my_int2

my_int1 += my_int2
print(my_int1)

my_int1 -= my_int2
print(my_int1)

my_int1 *= my_int2
print(my_int1)

my_int1 /= my_int2
print(my_int1)


# float

my_float1 = 1.2
my_float2 = 4.5

res = my_float1 + my_float2
print(res)


res = my_int2 + my_float2
print(res)

my_float1 = 0.1
my_float2 = 0.2

res = my_float1 + my_float2
print(res)

# print(0.3 == my_float1 + my_float2)

my_float1 = 0.00000000000000000003
print(my_float1)


my_float1_exp = 3e-20  # 3 * (10 ** -20)

# print(my_float1 / 0)


# boolean

my_bool1 = True
my_bool2 = False

my_bool = my_float1 > my_int1
print(my_bool)

# print(type(my_bool))
# print(type(my_int1))

my_bool = my_float1 < my_int1
print(my_bool)

my_bool = my_float1 >= my_int1
print(my_bool)

my_bool = my_float1 <= my_int1
print(my_bool)

my_bool = my_float1 == my_int1
print(my_bool)

my_bool = my_float1 != my_int1
print(my_bool)

# hello world

# hello world 2

