# # int float
#
# my_int = 100
#
# print(my_int)
# print(type(my_int))
#
# res = float(my_int)
# print(res)
# print(type(res))
#
#
# my_float = 1.9
#
# res = int(my_float)
# print(res)
# print(type(res))
#
#
# res = bool(0)
# print(res)
# print(type(res))
#
# res = bool(-10)
# print(res)
# print(type(res))
#
#
# # str
#
# my_str = 'qwertyxcvbn'
# print(my_str)
# print(type(my_str))
#
# my_str = ' '
# print(my_str)
# print(type(my_str))
#
# my_str = "qwer12345678\n@#$%^&*() tyxcvbn"
# print(my_str)
# print(type(my_str))
#
#
# my_str = '''qwerty
# asfsg
# afs
# xcvbn'''
#
# print(my_str)
# print(type(my_str))
#
#
# my_str = """qwerty
# asfsg \'
# afs
# xcvbn"""
#
# print(my_str)
# print(type(my_str))
#
#
# my_str = "qwert'yxcvbn"
# print(my_str)
# print(type(my_str))
#
#
# my_str = 'qwert\'yxcvbn \n'
# print(my_str)
# print(type(my_str))

# transform to str
#
# my_int = 10
#
# result = str(my_int)
# print(result)
# print(type(result))
#
# result = int('12345')
# print(result)
# print(type(result))
#
# result = float('123.45')
# print(result)
# print(type(result))
#
#
# my_str1 = 'qwertyuiop'
# my_str2 = '123456789'
#
# result = my_str1 + my_str2 + '!@#$%^&*('
# print(result)
# print(type(result))
#
# # result = my_str1 + 1234
# # print(result)
# # print(type(result))
#
# result = my_str1 * 3  # my_str1 + my_str1 + my_str1
#
# print(result)
# print(type(result))
#
# my_str1 *= 3
# print(my_str1)
# print(type(my_str1))


# Formatting
#
# name = 'Artem'
# age = 39
#
# # Hello, my name is Artem, i\'m 39.
#
# res = 'Hello, my name is ' + name + ', i\'m ' + str(age)
# print(res)
#
# res = 'Hello, my name is %s, i\'m %s' % (name, age)
# # res = 'Hello, my name is %s, i\'m %s' % (age, name)
# print(res)
#
# tpl = 'Hello, my name is %s, i\'m %s'
# result = tpl % (name, age)
# print(result)
#
# tpl = 'Hello, my name is {}, i\'m {}'
# result = tpl.format(name, age)
# print(result)
#
# tpl = 'Hello, my name is {name}, i\'m {age}'
# result = tpl.format(name=name, age=12234)
# print(result)
#
#
# # f-string
# name1 = 'Artem'
# age2 = 39
#
# result = f'Hello, my name is {name}, i\'m {age2}'
# print(result)
#
# result_len = len(result)
# print(result_len)
#
# res = 'Artem' in result
# print(res)


# if else

# condition = False
#
# if condition:
#     print(f'Inner code block IF {condition}')
#     print(f'Inner code block IF {condition}')
# else:
#     print(f'Inner code block ELSE {condition}')
#     print(f'Inner code block ELSE {condition}')
#
# a = 100
# b = 20
#
# if a > b:  # False
#     print('Inner code block IF a > b')
#     print('Inner code block IF a > b')
# else:
#     print('Inner code block ELSE a <= b')
#     print('Inner code block ELSE a <= b')


# a = 100
# b = 20
# res = a > b
#
# if res:  # False
#     print(f'Inner code block IF a > b')
#     print(f'Inner code block IF a > b')
# else:
#     print(f'Inner code block ELSE a <= b')
#     print(f'Inner code block ELSE a <= b')


# my_int = 0
#
#
# if my_int:  # bool(my_int) True
#     print('Inner code block IF ')
#     print('Inner code block IF ')
# else:
#     print('Inner code block ELSE')
#     print('Inner code block ELSE')


# my_str = ''
# if my_str:  # bool(my_str) True
#     print('Inner code block IF ')
#     print('Inner code block IF ')
# else:
#     print('Inner code block ELSE')
#     print('Inner code block ELSE')


# my_str = 'abc2'
#
# if my_str == 'abc':  # bool(my_str) True
#     print('Inner code block IF ')
#     print('Inner code block IF ')
# else:
#     print('Inner code block ELSE')
#     print('Inner code block ELSE')


# my_str = 'abc2'
#
# if 'ax' in my_str:
#     print('Inner code block IF ')
#     print('Inner code block IF ')
# else:
#     print('Inner code block ELSE')
#     print('Inner code block ELSE')
#

# my_condition = 'True'
#
# if my_condition is True:
#     print('Inner code block IF ')
#     print('Inner code block IF ')
# else:
#     print('Inner code block ELSE')
#     print('Inner code block ELSE')
#

# my_condition = 'True'
#
# if type(my_condition) == str:
#     print('Inner code block IF')
#     print('Inner code block IF ')
# else:
#     print('Inner code block ELSE')
#     print('Inner code block ELSE')
#

# my_condition = '1.0'
#
# if type(my_condition) == str:
#     print('Inner code block IF STR')
# elif type(my_condition) == bool:
#     print('Inner code block IF BOOL')
# elif type(my_condition) == int:
#     print('Inner code block IF INT')
# else:
#     print('Inner code block ELSE')
#
# print('out from if block')
#
# my_condition = '10'
#
# if type(my_condition) == str:
#     print('Inner code block IF STR')
# elif type(my_condition) == bool:
#     print('Inner code block IF BOOL')
# # elif type(my_condition) == int:
# #     print('Inner code block IF INT')
# # else:
# #     print('Inner code block ELSE')
#
# print('out from if block')
#
#
# my_condition = '10'
#
# if type(my_condition) == str:
#     if '1' in my_condition:
#         print('11111111111')
#     print('Inner code block IF STR')
#     print('Inner code block IF STR')
#     print('Inner code block IF STR')
# elif type(my_condition) == bool:
#     print('Inner code block IF BOOL')
#
# print('out from if block')
#
#
# a = 100
# b = 20
# c = 30
#
# # AND
#
# # if a > b and b < c and c > 40:
# #     print('a > b and b < c')
#
# a = 10
# b = 20
# c = 10
#
# # OR
#
# if a > b or b < c or c <= 10:
#     print('a > b and b < c')
#
# print('out from if block')
#
# a = 10
# b = 20
# c = 10
#
# # OR + AND
#
# if (a > b or b < c) and (c <= 10 or b > 2):
#     print('a > b and b < c')
#
# cond1 = a > b or b < c
# cond2 = c <= 10 or b > 2
#
# if cond1 and cond2:
#     print('a > b and b < c')
#
# # ()
# # **
# # * / // %
# # + -
# # == != > <
# # and
# # or
# # =
#
# res = (a > b or b < c) and (c <= 10 or b > 2)


# a = 10
# if a:
#     if a > 0:
#         pass
#     elif a < 0:
#         if 2 > 3:
#             pass
# elif 1 > 0:
#     pass
# else:
#     pass


# string methods
#
# my_str = '123456789230'
#
# res = my_str.replace('23', '++++')
# print(res)
#
# res = my_str.replace('23', '')
# print(res)
#
# my_str = '----12345----'
#
# res = my_str.strip('-')
# print(res)
#
# res = my_str.lstrip('-')
# print(res)
#
# res = my_str.rstrip('-')
# print(res)
#
# res = my_str.rstrip('-')
# print(res)
#
# my_str = 'abc1defGHI'
# res = my_str.upper()
# print(res)
#
# res = my_str.lower()
# print(res)
#
# res = my_str.title()
# print(res)
#
# my_str = 'abcabcabc'
# res = my_str.count('ab')
# print(res)
#
# my_str = 'abcabcabc'
# res = my_str.endswith('bcd')
# print(res)
#
# res = my_str.startswith('abc')
# print(res)
#
# my_str = '1234d56'
# res = my_str.isdigit()
# print(res)
#
# my_str = 'abcd1'
# res = my_str.isalpha()
# print(res)
#
# res = 'abcAd1'.isalnum()
# print(res)

var = 10

print('begin')

userinput = input('Enter smth: ')

if userinput.isdigit():
    age = int(userinput)
    if age > 400:
        pass

else:
    print('Wrong input')

print(type(userinput))

print(f'Your entered str is : {userinput}')

