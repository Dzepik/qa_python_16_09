# Ternary operator


# number = 123
#
# if number % 2 == 0:
#     is_even = True
# else:
#     is_even = False
#
#
# is_even = True if number % 2 == 0 else False
#
# # opt1 if cond else opt2
#
# # List
# # syntax
#
# my_list = [1, '2', True, None]
#
# print(type(my_list))
# print(my_list)
#
# my_list1 = list()
# print(my_list1)
#
# my_list2 = [3, 4, 5]
#
# res = my_list + my_list2
# print(res)
#
# res = res * 2
# print(res)
#
#
# # methods
#
# my_list = []
# print(my_list)
#
# my_list += [1]
# print(my_list)
#
# my_list += [3]
# print(my_list)
#

# my_list = []
# print(my_list)
# my_list.append(1)
# print(my_list)
# my_list.append('asdfu')
# print(my_list)
# my_list.append(True)
# print(my_list)

# print('asdfuw' in my_list)
#
# print(my_list.index('asdfu'))
# print(my_list[:2])
#
# print(my_list[1].upper())
# print(my_list[2])
#
# # None type
# my_none = None
#
#
# my_list = [1, 2.3, True, None, '0123456789', [1, 2, 3, 4], '0123456789']
#
# print(my_list[4][-1])
# print(type(my_list[4]))
#
# print(my_list[5][3])
#
# my_list[5].append(5)
#
# print(my_list)
# print(my_list[5].append([]))
# print(my_list)
#
# my_list.pop(4)
# print(my_list)
#
# my_list.remove('0123456789')
# print(my_list)
#
# my_list[0] = ['a', 'b', 'c']
# print(my_list)


# iteration (while)

# idx = 0
# my_list = [1, 2.3, True, None, '0123456789', [1, 2, 3, 4], '0123456789']
#
# while idx < len(my_list):
#     print('->', my_list[idx], type(my_list[idx]))
#     idx += 1
#
#
# idx = 0
# my_str = 'asdfghjkl;'
#
# while idx < len(my_str):
#     print('->', my_str[idx], type(my_str[idx]))
#     idx += 1


# loop for
#
# my_list = [1, 2.3, 'abc', True, None, '0123456789', 'abc', [1, 2, 3, 4], '0123456789']
#
# for elem in my_list:
#     if elem == 'abc':
#         continue
#
#     if type(elem) == list:
#         for inner_element in elem:
#             print('inner list element', inner_element)
#
#     print('->', elem, type(elem))
#
#     # if elem is True:
#     #     break
#
# user_data = []
#
# name = 'Artem'
# user_data.append(name)
#
# password = '1234567'
# user_data.append(password)
#
# age = 38
# user_data.append(age)
#
# print(user_data)
#
# user_pwd = user_data[1]

# lst = ['a', None, 'b', 'c', 'd']
# print(lst[0])
# print(lst[1])
# print(lst.index('a'))
#
#
# print(str(lst))
#
# print(bool(lst))
# print(bool([]))
#
# if lst:
#     pass
#
# # immutable
# a = 10
# print(id(a))
#
# a = a + 1
# print(id(a))
#

# # mutable
# lst = [1, 2, 3]
# print(id(lst))
#
# lst.append(4)
# print(id(lst))

# lst1 = [1, 2, 3]
#
# lst2 = lst1
#
# lst2.append(4)
#
# print(lst1)
# print(lst2)

#
# lst1 = [1, 2, 3]
#
# lst2 = lst1.copy()
#
# lst2.append(4)
#
# print(lst1)
# print(lst2)
#
#
# lst2[0] = 'asdfg'
#
# print(lst1)
# print(lst2)


# lst1 = [1, 2, ['a', 'b']]
#
# lst2 = lst1.copy()
#
# lst2.append(4)
# lst2[2].append('c')
#
# print(id(lst1[2]))
# print(id(lst2[2]))
#
# print(lst1)
# print(lst2)


# import copy
#
# lst1 = [1, 2, ['a', 'b']]
#
# lst2 = copy.deepcopy(lst1)
# lst2.append(4)
# lst2[2].append('c')
#
# print(id(lst1[2]))
# print(id(lst2[2]))
# print(lst1)
# print(lst2)
#
