# # DRY
#
# age = '12312'
#
# int_age = int(age)
# if int_age > 1:
#     pass
# elif int_age > 10:
#     pass
# elif int_age > 100:
#     pass
#
# # KISS
#
# # YAGNI
#
# # booleans is and not
# if age.isdigit() is False:
#     pass
#
# if not age.isdigit():  # not True -> False , not False -> True
#     pass


# loop
# while


# counter = 0
#
# while counter < 10:
#     print(f'Hello world - {counter}')
#     if counter % 2 == 0:
#         print('Match odd')
#     counter += 1
#
# print('end of loop')


# counter = 0
#
# while True:
#     print(f'Hello world - {counter}')
#     if counter % 2 == 0:
#         print('Match odd')
#     counter += 1
#
#     if counter > 10:
#         break
#
# print('end of loop')

# counter = 0
#
# while True:
#     counter += 1
#     if counter % 2 == 0:
#         continue
#     elif counter > 10:
#         break
#     print(f'Hello world - {counter}')
#
#     # if counter > 10:
#     #     break
#
# print('end of loop')

# while True:
#     user_input = input('Enter int, please!')
#     if user_input.isdigit():
#         print(f'Thanks for {user_input}')
#         break
#     else:
#         print(f'{user_input} - it is not int!')


# str

# my_str = '0123456789'
# my_str = 'asdfghjakl'
#
# print(my_str.find('as'))

#   access from zero
# my_str = 'asdfghjakl'
#
# print(my_str.find('a', 3, 9))
#
# print(my_str[0])
# print(my_str[9])
# # print(my_str[19])
# print(len(my_str))
# print(my_str[len(my_str)-1])
# print(my_str[-1])
# print(my_str[-2])


#   slices

# my_str = '0123456789'
#
# print(my_str)
# print('my_str[0]', my_str[0])
# print('my_str[2:5]', my_str[2:5])
# print('my_str[3:15]', my_str[3:15])
# print('my_str[-7:-3]', my_str[-7:-3])
# print('my_str[-1:4]', my_str[-1:4])
# print('my_str[0:9:3]', my_str[0:9:3])
# print('my_str[9:0:-1]', my_str[9:0:-1])
#
# print('my_str[:5]', my_str[:5])
# print('my_str[4:]', my_str[4:])
# print('my_str[4::2]', my_str[4::2])
# print('my_str[:8:2]', my_str[:8:2])
# print('my_str[::-1]', my_str[::-1])
#
# a = 1
# b = 2
# c = -1
# print('my_str[a:b:c]', my_str[a:b:c])
#
# print('my_str[:len(my_str)//2]', my_str[len(my_str)//2:])


# try except
# userinput = input('Enter int')
#
# try:
#     userinput = int(userinput)
# except:
#     print('It\'s not an int!')
#
# print('Done!')


# userinput = input('Enter int')
#
# try:
#     userinput = int(userinput)
#     userinput = 10 / userinput
# except ZeroDivisionError:
#     print('It\'s a ZERO!')
# except ValueError:
#     print('It\'s not an int!')
#
# print('Done!')

# userinput = input('Enter int')
#
# try:
#     userinput = int(userinput)
#     userinput = 10 / userinput
# except Exception as error:
#     print(f'Exception: {type(error)}, {error}')
#
#
# print('Done!')


# userinput = input('Enter int')
#
# try:
#     userinput = int(userinput)
#     userinput = 10 / userinput
# except ZeroDivisionError as error:
#     print(f'Exception: {type(error)}, {error}')
# except:
#     print('Error')
#
# print('Done!')
#

# userinput = input('Enter int')
#
# try:
#     userinput = int(userinput)
#     userinput = 10 / userinput
# except (ZeroDivisionError, ValueError) as error:
#     print(f'Exception: {type(error)}, {error}')
# except:
#     print('Error')
#
# print('Done!')


# userinput = input('Enter int')
#
# try:
#     userinput = int(userinput)
#     userinput = 10 / userinput
# except:
#     print('Error')
# else:
#     print('Without error!')
#
#
# print('Done!')


# userinput = input('Enter int')
#
# try:
#     userinput = int(userinput)
#     userinput = 10 / userinput
# except:
#     print('Error')
# else:
#     print('Without error!')
# finally:
#     print('Final code')
#
#
# print('Done!')

# counter = 0
#
# while True:
#     counter += 1
#
#     while True:
#         user_input = input('Give me float: ')
#
#         try:
#             result = float(user_input)
#         except ValueError as e:
#             print(f'It\'s not a float: {user_input}')
#         else:
#             print('Thnx!')
#             break
#
#     if counter >= 3:
#         break


counter = 13

while counter < 5:
    print(f'Inside WHILE {counter}')
    counter += 1
else:
    print(f'Inside ELSE {counter}')
